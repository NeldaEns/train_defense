using UnityEngine;

namespace D2D.Gameplay
{
    public interface ISpawner
    {
        GameObject Spawn();
    }
}