﻿using System;
using Cinemachine;
using D2D.Gameplay;
using UnityEngine;
using DG.Tweening;

namespace D2D.Cameras
{
    public class CameraPlayerAutoFollow : MonoBehaviour
    {
        [SerializeField] private bool _follow;
        [SerializeField] private bool _lookAt;

        [SerializeField] private Transform player;
        [SerializeField] private float speed;
        private Transform camera;
        private Vector3 offset;


        //private void OnValidate()
        //{
        //    UpdateCameraTarget();
        //}

        private void Awake()
        {
            camera = this.transform;
        }

        private void Start()
        {
            player = FindAnyObjectByType<Player>().transform;
            offset = camera.position -= player.position;
            //UpdateCameraTarget();
        }

        private void Update()
        {
            Follow();
        }

        private void UpdateCameraTarget()
        {
            var vcam = GetComponent<CinemachineVirtualCamera>();
            var player = FindObjectOfType<Player>();

            if (vcam == null || player == null)
                return;

            if (_follow)
                vcam.Follow = player.transform;

            if (_lookAt)
                vcam.LookAt = player.transform;
        }

        private void Follow()
        {
            camera.DOMoveX(player.position.x + offset.x, speed * Time.deltaTime);
            camera.DOMoveZ(player.position.z + offset.z, speed * Time.deltaTime);
        }
    }
}
