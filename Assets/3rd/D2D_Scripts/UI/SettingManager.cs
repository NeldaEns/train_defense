using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class SettingManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnDisable()
    {
        transform.GetChild(0).localScale = Vector3.zero;
    }

    private void OnEnable()
    {
        transform.GetChild(0).DOScale(Vector3.one, .5f);
    }
}
