namespace D2D.Utilities
{
    /// <summary>
    /// All objects of this type can be created by hand or dynamically when needed.
    /// Also ILazyCreating must exists only in ONE instance (like singleton).
    /// </summary>
    public interface ILazy
    {
        
    }
}