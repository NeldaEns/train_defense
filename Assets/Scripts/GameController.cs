using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public static GameController ins;


    public GameObject enemy;


    private void Awake()
    {
        if(ins != null)
        {
            Destroy(gameObject);
        }    
        else
        {
            ins = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    private void Start()
    {
        GameObject enemy1 = Instantiate(enemy, transform.position, Quaternion.identity);
    }
}
