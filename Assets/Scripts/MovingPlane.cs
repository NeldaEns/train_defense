﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlane : MonoBehaviour
{
    public Transform positionA; 
    public Transform positionB; 
    public float moveSpeed = 150f;

    void Start()
    {
        StartCoroutine(MoveAndResetCoroutine());
    }

    IEnumerator MoveAndResetCoroutine()
    {
        while (true)
        {
            
            yield return MoveToPosition(positionA.position);

            
            transform.position = positionB.position;

            
            yield return null;
        }
    }

    IEnumerator MoveToPosition(Vector3 targetPosition)
    {
        while (transform.position != targetPosition)
        {
            
            transform.position = Vector3.MoveTowards(transform.position, targetPosition, moveSpeed * Time.deltaTime);

            yield return null;
        }
    }
}
