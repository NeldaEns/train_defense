﻿internal interface IHittable
{
    public void GetHit(float damage);
}